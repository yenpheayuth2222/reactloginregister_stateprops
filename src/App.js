
import 'bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import Nav from './component/Nav';
import Home from './component/Home';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Login from './component/Login';
import Register from './component/Register';

import React, { Component } from 'react'

export default class App extends Component {
  render() {
    return (
      <BrowserRouter>
      <div>
        <Nav/>
  
        <div className="auth-wrapper">
          <div className="auth-inner"> 
              <Switch>
                <Route exact path="/" component={Home}/>
                <Route  path="/Login" component={Login}/>
                <Route path="/Register" component={Register}/>
              </Switch>
          </div>
        </div>
  
      </div>
      </BrowserRouter>
    )
  }
}

