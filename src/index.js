import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Axios from 'axios';

Axios.defaults.baseURL='http://35.240.153.61:8080/api/v1/auth/';

ReactDOM.render(<App />, document.getElementById('root'));