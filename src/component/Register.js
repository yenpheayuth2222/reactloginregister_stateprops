import Axios from 'axios';
import React, { Component } from 'react'


export default class Register extends Component {

    handleSubmit = e=>{
        e.preventDefault();
        const data= {
            email: this.email,
            username: this.username,
            password: this.password
        }
        // signup(data)
        Axios.post('register',data)
        .then(res=>{
            console.log(res);
        }).catch(error=>{
            console.log(error);
        })
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit} id="template">
                    <h3>Register</h3>

                    <div className="form-group">
                        <label>Email</label>
                        <input type="email" className="form-control" placeholder="Email"
                             onChange={e=>this.email= e.target.value}
                        />
                    </div>

                    <div className="form-group">
                        <label>Username</label>
                        <input type="text" className="form-control" placeholder="Username"
                             onChange={e=>this.username= e.target.value}
                        />
                    </div>

                    <div className="form-group">
                        <label>Password</label>
                        <input type="password" className="form-control" placeholder="Password"
                             onChange={e=>this.password= e.target.value}
                             autoComplete=""
                        />
                    </div>
                    <button className="btn btn-primary btn-block"
                    >Sign Up</button>
                </form>
            </div>
        )
    }
}
