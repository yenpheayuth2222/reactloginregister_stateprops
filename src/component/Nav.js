import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';
import Login from './Login';
import Register from './Register';
import Home from './Home';


export default class Nav extends Component {
    render() {
        return (
            <div>
                <nav className="navbar navbar-expand navbar-light fixed-top">
                    <div className="container">
                    <Link to={'/'} className="navbar-brand">Home</Link>
                    <div className="collapse navbar-collapse">
                        <ul className="navbar-nav ml-auto">
                        <li className="nav-item">
                            <Link to={'/Login'} className="nav-link">Login</Link>
                        </li>
                        <li className="nav-item">
                            <Link to={'/Register'} className="nav-link">Sign Up</Link>
                        </li>
                        </ul>
                    </div>
                    </div>
                </nav>
            </div>
        )
    }
}
