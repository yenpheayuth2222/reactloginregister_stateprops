import Axios from 'axios';
import React, { Component } from 'react'
import { Redirect } from 'react-router-dom';

export default class Login extends Component {
    

    handleSubmit = e=>{
        e.preventDefault();
        const data= {
            usernameOrEmail: this.usernameOrEmail,
            password: this.password
        }
        // signin(data)
        Axios.post('login',data)
        .then(res=>{
            console.log(res);
            localStorage.getItem('token', res.data.token);
            this.setState({
                loggedIn: true
            })
        }).catch(error=>{
            // console.log(error);
            this.setState({
                message: error.response.data.message
            })
        })
    }

    render() {
        
        return (
            <div>
                <form onSubmit={this.handleSubmit} id="template">
                   
                    <h3>MyFace Login</h3>

                    <div className="form-group">
                        <label>Username or Email</label>
                        <input type="text" className="form-control" placeholder="UsernameOrPassword"
                             onChange={e=>this.usernameOrEmail= e.target.value}
                             required="Please enter email"
                        />
                    </div>

                    <div className="form-group">
                        <label>Password</label>
                        <input type="password" className="form-control" placeholder="Password"
                             onChange={e=>this.password= e.target.value}
                             autoComplete=""
                             required="password"
                        />
                    </div>
                    <button className="btn btn-primary btn-block"
                    >Login</button>
                </form>
            </div>
        )
    }
}
