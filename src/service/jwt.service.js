import axios from "axios";

const JWT_API = axios.create({
    baseURL: "http://35.240.153.61:8080/api/v1"
})

export const signup = async (user)=>{
    try {
        const result = await JWT_API.post("/auth/register", user)
        console.log("sign up: ", result);
        return result
    } catch (error) {
        console.log("signup err: ", error);
    }
}

export const signin = async (user)=>{
    try {
        const result = await JWT_API.post("/auth/login", user)
        console.log("sign in: ", result.data);
        return  result.data
    } catch (error) {
        console.log("signin err: ", error);
    }
}